using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float maxSpeed;
    [SerializeField] private float accelleration;
    [SerializeField] private float decelleration;
    [SerializeField] private float jumpSpeed;
    [SerializeField, Min(1)] private int maxJumps = 2;

    [Header("GroundCheck")]
    [SerializeField] private Vector3 groundCheckDistance;
    [SerializeField] private Vector2 groundCheckSize;
    [SerializeField] LayerMask groundMask;

    private float direction;
    private bool jumpRequested;

    private Rigidbody2D rb;
    private Vector2 velocity;
    private bool isGrounded;
    private int onAirJumpCount;

    private void Awake()
    {
        direction = 0f;
        jumpRequested = false;
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        velocity = rb.velocity;

        GroundCheck();
        HorizontalMovement();
        
        if (jumpRequested)
            Jump();

        rb.velocity = velocity;
    }

    private void HorizontalMovement()
    {
        if (direction != 0f)
        {
            // Accelleration
            velocity.x = Mathf.MoveTowards(velocity.x, direction * maxSpeed, accelleration * Time.fixedDeltaTime);
        }
        else
        {
            // Decelleration
            velocity.x = Mathf.MoveTowards(velocity.x, 0f, decelleration * Time.fixedDeltaTime);
        }
    }

    private bool wasGrounded;
    private void GroundCheck()
    {
        isGrounded = Physics2D.OverlapBox(transform.position + groundCheckDistance, groundCheckSize, 1f, groundMask) != null;
        
        if (isGrounded && !wasGrounded)
            onAirJumpCount = 0;

        wasGrounded = isGrounded;
    }

    private void Jump()
    {

        if (isGrounded || onAirJumpCount < maxJumps)
        {
            velocity.y = jumpSpeed;
            onAirJumpCount++;
        }

        jumpRequested = false;
    }

    #region Input
    private void ChangeDirection(float direction)
    {
        this.direction = direction; 
    }

    private void JumpInput()
    {
        jumpRequested = true;
    }

    private void OnEnable()
    {
        InputManager.OnDirectionChanged += ChangeDirection;
        InputManager.OnJumpPressed += JumpInput;
    }

    private void OnDisable()
    {
        InputManager.OnDirectionChanged -= ChangeDirection;
        InputManager.OnJumpPressed -= JumpInput;
    }

    #endregion


#if UNITY_EDITOR
    [Header("Debug")]
    [SerializeField] private bool showGizmos = true;
    private void OnDrawGizmos()
    {
        if (!showGizmos) return;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + groundCheckDistance, groundCheckSize);
    }
#endif
}
