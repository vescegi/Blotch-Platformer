using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private Vector3 attackPosition;
    [SerializeField] private Vector2 attackSize;
    [SerializeField] private LayerMask layerMask;

    public void Attack()
    {
        Collider2D[] enemiesHit = Physics2D.OverlapBoxAll(transform.position + attackPosition, attackSize, 0f, layerMask);
        Debug.Log(enemiesHit.Length);
        if (enemiesHit.Length <= 0) return;

        float closestDistance = Mathf.Infinity;
        Collider2D closest = null;
        foreach (Collider2D c in enemiesHit)
        {
            float distance = Vector3.Distance(c.transform.position, transform.position);

            if(distance < closestDistance)
            {
                closestDistance = distance;
                closest = c;
            }
        }

        closest.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        InputManager.OnAttackPressed += Attack;
    }

    private void OnDisable()
    {
        InputManager.OnAttackPressed -= Attack;
    }

#if UNITY_EDITOR
    [Header("Debug")]
    [SerializeField] private bool showGizmos;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position + attackPosition, attackSize);
    }
#endif
}
