using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWander : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private LayerMask groundMask;

    private float direction = -1f;

    private float minX, maxX;


    private void Awake()
    {
        Transform platformStanding = GetPlatform();
        
        float platformX = platformStanding.position.x;
        float platformHalfSize = platformStanding.localScale.x / 2;
        float halfSize = transform.transform.localScale.x / 2;

        minX = platformX - platformHalfSize + halfSize;
        maxX = platformX + platformHalfSize - halfSize;

    }

    private void Update()
    {
        Vector3 position = transform.position;

        position.x += direction * speed * Time.deltaTime;
        position.x = Mathf.Clamp(position.x, minX, maxX);

        if(position.x == minX || position.x == maxX)
        {
            direction = -direction;
        }

        transform.position = position;
    }

    private Transform GetPlatform()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, -transform.up, 5f, groundMask);

        return hitInfo.transform;
    }
}
