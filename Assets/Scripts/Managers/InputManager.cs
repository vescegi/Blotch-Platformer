using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static event Action<float> OnDirectionChanged;
    public static event Action OnJumpPressed;
    public static event Action OnAttackPressed;

    private float _direction;
    private float Direction
    {
        get
        {
            return _direction;
        }
        set
        {
            _direction = value;
            OnDirectionChanged?.Invoke(_direction);
        }
    }

    private void Awake()
    {
        Direction = 0f;
    }

    public void LeftButtonPressed()
    {
        Direction -= 1;
    }

    public void LeftButtonReleased()
    {
        Direction += 1;
    }

    public void RightButtonPressed()
    {
        Direction += 1;
    }
    public void RightButtonReleased()
    { 
        Direction -= 1;
    }

    public void JumpButtonPressed()
    {
        OnJumpPressed?.Invoke();
    }

    public void AttackButtonPressed()
    {
        OnAttackPressed?.Invoke();
    }
}
