using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get => instance;
        set
        {
            if(instance == null)
            {
                instance = value;
                value.transform.parent = null;
                DontDestroyOnLoad(value);
            }
            else
            {
                Destroy(value);
            }
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        EnemyHit.OnPlayerHit += GameOver;
    }

    private void OnDisable()
    {
        EnemyHit.OnPlayerHit -= GameOver;
    }

    public void GameOver()
    {
        Debug.Log("GameOver");
    }
}
