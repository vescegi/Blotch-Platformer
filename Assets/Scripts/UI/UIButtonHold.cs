using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIButtonHold : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] UnityEvent OnButtonPressed;
    [SerializeField] UnityEvent OnButtonReleased;

    public void OnPointerDown(PointerEventData eventData)
    {
        OnButtonPressed.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnButtonReleased.Invoke();
    }
}
