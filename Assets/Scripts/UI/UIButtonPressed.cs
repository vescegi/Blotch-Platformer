using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIButtonPressed : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] UnityEvent OnButtonPressed;

    public void OnPointerDown(PointerEventData eventData)
    {
        OnButtonPressed?.Invoke();
    }
}
