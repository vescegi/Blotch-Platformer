using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableUI : MonoBehaviour
{
    private void Awake()
    {
#if UNITY_EDITOR
        UnityEngine.Rendering.DebugManager.instance.enableRuntimeUI = false;
#endif
    }
}
